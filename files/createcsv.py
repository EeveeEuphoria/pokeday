#Creates the natdex.csv file if it does not already exist

import pokebase as pb
import csv

rows = []

for number in range(1, 899):
    namer = pb.pokemon_species(number).names
    writer = [str(number)]
    names = []
    for item in namer: #convert to easy to use list
        names.append([str(item.language), str(item.name)])

    for name in names:
        if name[0] == "en":
            writer.insert(1, name[1])
            print(name[1])
        elif name[0] == "ja":
            writer.insert(2, name[1])
            print(name[1])
        if len(writer) > 3:
            break
    
    rows.append(writer)

print(rows)

fields = ['No.', 'Name', 'Name(JPN)']

try:
    shutil.copyfile("files/natdex.csv", "files/natdex.csv.bak")
    print("Backed up original missing.csv file!")
except:
    pass

with open("files/natdex.csv", 'w') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(fields)
    csvwriter.writerows(rows)
    csvfile.close()